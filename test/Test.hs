import InstantInsanity.TypeLevel as TL
import InstantInsanity.PlainHaskell as PH

import Test.Tasty
import Test.Tasty.SmallCheck as SC
import Test.Tasty.QuickCheck as QC
import Test.Tasty.HUnit

main = defaultMain tests

tests :: TestTree
tests = testGroup "InstantInsanity" [plainHaskell, typeLevel]

plainHaskell = testGroup "PlainHaskell" [
	testCase "solution" $ (PH.solutions PH.cubes) @?= [
		[[G,B,W,R,B,G],[W,G,B,W,R,R],[R,W,R,B,G,R],[B,R,G,G,W,W]],
		[[G,B,R,W,B,G],[R,R,W,B,G,W],[R,G,B,R,W,R],[W,W,G,G,R,B]],
		[[G,W,R,B,B,G],[W,B,W,R,G,R],[R,R,B,G,W,R],[B,G,G,W,R,W]],
		[[G,B,B,R,W,G],[R,G,R,W,B,W],[R,W,G,B,R,R],[W,R,W,G,G,B]],
		[[G,R,B,B,W,G],[W,W,R,G,B,R],[R,B,G,W,R,R],[B,G,W,R,G,W]],
		[[G,W,B,B,R,G],[R,B,G,R,W,W],[R,R,W,G,B,R],[W,G,R,W,G,B]],
		[[G,B,B,W,R,G],[W,R,G,B,W,R],[R,G,W,R,B,R],[B,W,R,G,G,W]],
		[[G,R,W,B,B,G],[R,W,B,G,R,W],[R,B,R,W,G,R],[W,G,G,R,W,B]]
	]]

typeLevel = testGroup "TypeLevel" [
	testCase "solution" $ (TL.solutions (u :: TL.Cubes) @?= u)
	]