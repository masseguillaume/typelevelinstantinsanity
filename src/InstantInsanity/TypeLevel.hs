{-# LANGUAGE EmptyDataDecls
           , MultiParamTypeClasses
           , FlexibleInstances
           , FunctionalDependencies
           , TypeOperators
           , UnicodeSyntax
           , UndecidableInstances #-}

module InstantInsanity.TypeLevel where

import Prelude hiding (all, flip, map, filter, and)

data R
data G
data B
data W

data Cube u f r b l d

type Cube1 = Cube B G W G B R
type Cube2 = Cube W G B W R R
type Cube3 = Cube G W R B R R
type Cube4 = Cube B R G G W W

type RedCube = Cube R R R R R R
type BlueCube = Cube B B B B B B

u = undefined

data True
data False

class And b1 b2 b | b1 b2 → b where
  and :: b1 → b2 → b

instance And True True True where and = u
instance And True False False where and = u
instance And False True False where and = u
instance And False False False where and = u

data Nil
data Cons x xs

data x ::: xs
infixr 5 :::

class ListConcat l1 l2 l | l1 l2 → l where
  listConcat :: l1 → l2 → l

instance ListConcat Nil l l
  where listConcat = u

instance (ListConcat xs ys zs) ⇒ ListConcat (x ::: xs) ys (x ::: zs)
  where listConcat = u

class Apply f a b | f a → b
  where apply :: f → a → b

data Rotation
data Twist
data Flip

instance Apply Rotation (Cube u f r b l d) (Cube u r b l f d) where
  apply = u

instance Apply Twist (Cube u f r b l d) (Cube f r u l d b) where
  apply = u

instance Apply Flip (Cube u f r b l d) (Cube d l b r f u) where
  apply = u

class Map f xs zs | f xs → zs where
  map ::f → xs → zs

instance Map f Nil Nil where
  map = u

instance (Apply f x z, Map f xs zs) ⇒ Map f (x ::: xs) (z ::: zs) where
  map = u

class Filter f xs zs | f xs → zs where
  filter :: f → xs → zs

instance Filter f Nil Nil where
  filter = u

class AppendIf b x ys zs | b x ys → zs
instance AppendIf True x ys (x ::: ys)
instance AppendIf False x ys ys

instance (Apply f x b, Filter f xs ys, AppendIf b x ys zs) ⇒
  Filter f (x ::: xs) zs where
  filter = u

class MapAppend f xs zs | f xs → zs where
  mapAppend :: f → xs → zs

instance MapAppend f Nil Nil where
  mapAppend = u

instance (Map f xs ys, ListConcat xs ys zs) ⇒ MapAppend f xs zs where
  mapAppend = u

class MapAppend2 f xs zs | f xs → zs where
  mapAppend2 :: f → xs → zs

instance MapAppend2 f Nil Nil where
  mapAppend2 = u

instance (Map f xs ys, MapAppend f ys ys', ListConcat xs ys' zs) ⇒
  MapAppend2 f xs zs where
  mapAppend2 = u

class MapAppend3 f xs zs | f xs → zs where
  mapAppend3 :: f → xs → zs

instance MapAppend3 f Nil Nil where
  mapAppend3 = u

instance (Map f xs ys, MapAppend2 f ys ys', ListConcat xs ys' zs) ⇒
  MapAppend3 f xs zs where
  mapAppend3 = u

data Orientations

instance (MapAppend Flip (c ::: Nil ) fs, MapAppend2 Twist fs ts,
    MapAppend3 Rotation ts zs) ⇒ Apply Orientations c zs where
    apply = u

class NE x y b | x y → b where
  ne :: x → y → b

instance NE R R False where ne = u
instance NE B B False where ne = u
instance NE G G False where ne = u
instance NE W W False where ne = u

instance NE R G True where ne = u
instance NE R B True where ne = u
instance NE R W True where ne = u
instance NE G R True where ne = u
instance NE G B True where ne = u
instance NE G W True where ne = u
instance NE B R True where ne = u
instance NE B G True where ne = u
instance NE B W True where ne = u
instance NE W R True where ne = u
instance NE W G True where ne = u
instance NE W B True where ne = u

class All l b | l → b where
  all :: l → b

instance All Nil True where
  all = u

instance All (False ::: xs) False where
  all = u

instance (All xs b) ⇒ All (True ::: xs) b where
  all = u

class Compatible c1 c2 b | c1 c2 → b where
  compatible :: c1 → c2 → b

instance (
  NE f1 f2 bF, NE r1 r2 bR, NE b1 b2 bB, NE l1 l2 bL,
  All (bF ::: bR ::: bB ::: bL ::: Nil) b)
  ⇒ Compatible (Cube u1 f1 r1 b1 l1 d1) (Cube u2 f2 r2 b2 l2 d2) b where
  compatible = u

class Allowed c cs b | c cs → b where
  allowed :: c → cs → b

instance Allowed c Nil True where
  allowed = u

instance (
  Compatible c y b1 , Allowed c ys b2 , And b1 b2 b) ⇒
  Allowed c (y ::: ys) b where
  allowed = u

class Solutions cs ss | cs → ss where
  solutions :: cs → ss

instance Solutions Nil (Nil ::: Nil) where
  solutions = u

instance (
  Solutions cs sols, Apply Orientations c os, AllowedCombinations os sols zs) ⇒
  Solutions (c ::: cs) zs where
  solutions = u

class AllowedCombinations os sols zs | os sols → zs

instance AllowedCombinations os Nil Nil

instance (
  AllowedCombinations os sols as, MatchingOrientations os s bs,
    ListConcat as bs zs) ⇒
  AllowedCombinations os (s ::: sols) zs

class MatchingOrientations os sol zs | os sol → zs

instance MatchingOrientations Nil sol Nil

instance (
  MatchingOrientations os sol as, Allowed o sol b,
    AppendIf b (o ::: sol) as zs) ⇒
  MatchingOrientations (o ::: os) sol zs

type Cubes = (Cube1 ::: Cube2 ::: Cube3 ::: Cube4 ::: Nil)