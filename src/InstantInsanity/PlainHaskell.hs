module InstantInsanity.PlainHaskell where

import Prelude hiding (flip)

data Color = B | G | R | W deriving (Show, Eq)

cubes = [[B,G,W,G,B,R], [W,G,B,W,R,R], [G,W,R,B,R,R], [B,R,G,G,W,W]]

-- Rotate a cube 90 degrees over its Z-axis, leaving up and down in place.
rot [u, f, r, b, l, d] = [u, r, b, l, f, d]

-- Twist a cube around the axis running from the upper-front-right
-- corner to the back-left-down corner.
twist [u, f, r, b, l, d] = [f, r, u, l, d, b]

-- Exchange up and down, front and left, back and right.
flip [u, f, r, b, l, d] = [d, l, b, r, f, u]

-- Compute all 24 ways to orient a cube.
orientations c =
  [c''' | c' <- [c, rot c, rot (rot c), rot (rot (rot c))],
          c'' <- [c', twist c', twist (twist c')],
          c''' <- [c'', flip c'']]

-- Compute which faces of a cube are visible when placed in a pile.
visible [u, f, r, b, l, d] = [f, r, b, l]

-- Two cubes are compatible if they have different colours on every
-- visible face.
compatible c c' = and [x /= x' | (x, x') <- zip (visible c) (visible c')]

-- Determine whether a cube can be added to pile of cubes, without
-- invalidating the solution.
allowed c cs = and [compatible c c' | c' <- cs]

-- Return a list of all ways of orienting each cube such that no side of
-- the pile has two faces the same.
solutions [] = [[]]
solutions (c:cs) = [c' : cs' | cs' <- solutions cs,
	c' <- orientations c,
	allowed c' cs']